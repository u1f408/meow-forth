#![allow(unused_imports)]

extern crate meow_forth;
use meow_forth::{
    ast::{parser::AstParser, AstNode, AstNodeType},
    Interpreter,
};

pub static COMPLEX: &'static str = r#"
    : test ( n -- )
        :: test_print ( n -- )
            FFh and dup
            8 > if
                . cr
            then
        ;

        test_print
    ;

    4 test
    32 test

    -1 if
        ." Hello, \"world\" :3" cr
    else
        ." This should never be displayed!" cr
    then
"#;

#[test]
fn complex_parse() {
    let parser = AstParser::new();
    let output = parser.parse(COMPLEX);

    // Check for validity and unwrap
    assert!(output.is_ok());
    let output = output.unwrap();
    assert_eq!(output.len(), 7);

    // Child 0: Nested function definition
    assert_eq!(output[0].node_type, AstNodeType::FunctionDef);
    assert_eq!(output[0].children.len(), 2);
    assert_eq!(output[0].children[0].node_type, AstNodeType::FunctionDef);
    assert_eq!(output[0].children[0].children.len(), 6);

    // Children 1 and 2: Call to nested function definition
    // This call should print nothing.
    assert_eq!(output[1].node_type, AstNodeType::Integer);
    assert_eq!(output[1].node_value, 4);
    assert_eq!(output[2].node_type, AstNodeType::Word);
    assert_eq!(output[2].node_name, "test".to_string());

    // Children 3 and 4: Call to nested function definition
    // This call should print something.
    assert_eq!(output[3].node_type, AstNodeType::Integer);
    assert_eq!(output[3].node_value, 32);
    assert_eq!(output[4].node_type, AstNodeType::Word);
    assert_eq!(output[4].node_name, "test".to_string());

    // Children 5 and 6: Conditional
    assert_eq!(output[5].node_type, AstNodeType::Integer);
    assert_eq!(output[5].node_value, -1);
    assert_eq!(output[6].node_type, AstNodeType::Conditional);
    assert_eq!(output[6].children.len(), 2);
    assert_eq!(
        output[6].children[0].node_type,
        AstNodeType::ConditionalTrueBranch
    );
    assert_eq!(output[6].children[0].children.len(), 2);
    assert_eq!(
        output[6].children[1].node_type,
        AstNodeType::ConditionalFalseBranch
    );
    assert_eq!(output[6].children[1].children.len(), 2);
}

#[test]
fn complex_execute() {
    let parser = AstParser::new();
    let output = parser.parse(COMPLEX).unwrap();
    let mut interpreter = Interpreter::new();
    let res = interpreter.execute(output).unwrap();

    let res = res
        .iter()
        .map(|x| x.to_string())
        .filter(|x| x != &String::new())
        .collect::<Vec<String>>();

    let expected = ["32", "\n", "Hello, \"world\" :3", "\n"]
        .iter()
        .map(|x| x.to_string())
        .collect::<Vec<String>>();

    assert_eq!(res, expected);
}
