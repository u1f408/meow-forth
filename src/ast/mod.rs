//! Abstract syntax tree handling.

use crate::alloc_prelude::*;

mod error;
pub use self::error::ParseError;
pub mod b3;
pub mod parser;

/// Available AST node types.
#[repr(usize)]
#[derive(Debug, Clone, PartialEq)]
pub enum AstNodeType {
    /// Unknown node type.
    Unknown = 0,

    /// A standalone word.
    ///
    /// The `node_name` of the node is the word.
    Word = 1,

    /// An integer.
    ///
    /// The `node_value` of the node is the integer value.
    Integer = 2,

    /// A string.
    ///
    /// The `node_name` of the node is the content of the string.
    String = 3,

    /// A function definition.
    ///
    /// The `node_name` of the node is the name of the function, and the
    /// `children` of the node are the parsed values of the interior of
    /// the function.
    FunctionDef = 4,

    /// A conditional (`if [...] else [...] then`).
    ///
    /// A `Conditional` node always has two children, the first of which is
    /// a `ConditionalTrueBranch`, and the second a `ConditionalFalseBranch`.
    Conditional = 5,

    /// The truthy branch of a conditional.
    ///
    /// The `children` of the node are the parsed values of the interior of
    /// the branch.
    ConditionalTrueBranch = 6,

    /// The falsy branch of a conditional.
    ///
    /// The `children` of the node are the parsed values of the interior of
    /// the branch.
    ConditionalFalseBranch = 7,

    /// The body of a `do [...] loop` loop.
    ///
    /// Note that this is just the body of the loop, and does not include the
    /// start and end values of the loop - these are pulled from the stack
    /// at execution.
    ///
    /// The `children` of the node are the parsed values of the interior of
    /// the loop.
    DoLoopBody = 8,

    /// The body of a `begin [...] until` loop.
    ///
    /// The `children` of the node are the parsed values of the interior of
    /// the loop.
    UntilLoopBody = 9,
}

impl From<u64> for AstNodeType {
    fn from(i: u64) -> Self {
        match i {
            1 => Self::Word,
            2 => Self::Integer,
            3 => Self::String,
            4 => Self::FunctionDef,
            5 => Self::Conditional,
            6 => Self::ConditionalTrueBranch,
            7 => Self::ConditionalFalseBranch,
            8 => Self::DoLoopBody,
            9 => Self::UntilLoopBody,
            _ => Self::Unknown,
        }
    }
}

impl From<usize> for AstNodeType {
    fn from(i: usize) -> Self {
        Self::from(i as u64)
    }
}

/// An AST node.
#[derive(Debug, Clone, PartialEq)]
pub struct AstNode {
    pub node_type: AstNodeType,
    pub node_name: String,
    pub node_value: isize,
    pub children: Vec<AstNode>,
}

impl AstNode {
    /// Creates an AST node containing a single word.
    pub fn new_word(word: String) -> Self {
        Self {
            node_type: AstNodeType::Word,
            node_name: word,
            node_value: 0,
            children: Vec::new(),
        }
    }

    /// Creates an AST node containing a single integer.
    pub fn new_integer(value: isize) -> Self {
        Self {
            node_type: AstNodeType::Integer,
            node_name: String::new(),
            node_value: value,
            children: Vec::new(),
        }
    }

    /// Creates an AST node containing a string.
    pub fn new_string(value: String) -> Self {
        Self {
            node_type: AstNodeType::String,
            node_name: value,
            node_value: 0,
            children: Vec::new(),
        }
    }

    /// Creates an AST node for a conditional with the given `Vec<AstNode>`s
    /// for the true and false branches.
    pub fn new_conditional(if_true: Vec<Self>, if_false: Vec<Self>) -> Self {
        let true_branch = Self {
            node_type: AstNodeType::ConditionalTrueBranch,
            node_name: String::new(),
            node_value: 0,
            children: if_true,
        };

        let false_branch = Self {
            node_type: AstNodeType::ConditionalFalseBranch,
            node_name: String::new(),
            node_value: 0,
            children: if_false,
        };

        Self {
            node_type: AstNodeType::Conditional,
            node_name: String::new(),
            node_value: 0,
            children: [true_branch, false_branch].to_vec(),
        }
    }

    /// Creates an AST node containing a function definition, using the given
    /// name and the `Vec<AstNode>` of the function contents.
    pub fn new_function(global: bool, name: &str, children: Vec<Self>) -> Self {
        Self {
            node_type: AstNodeType::FunctionDef,
            node_name: name.to_string(),
            node_value: if global { 0 } else { 1 },
            children,
        }
    }

    /// Creates an AST node containing a do...loop loop definition, using
    /// the given `Vec<AstNode>` as the contents.
    pub fn new_do_loop(children: Vec<Self>) -> Self {
        Self {
            node_type: AstNodeType::DoLoopBody,
            node_name: String::new(),
            node_value: 0,
            children,
        }
    }

    /// Creates an AST node containing a begin...until loop definition, using
    /// the given `Vec<AstNode>` as the contents.
    pub fn new_until_loop(children: Vec<Self>) -> Self {
        Self {
            node_type: AstNodeType::UntilLoopBody,
            node_name: String::new(),
            node_value: 0,
            children,
        }
    }
}
