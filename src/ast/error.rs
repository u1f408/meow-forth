use core::fmt::{self, Display};

/// Error type for AST parse operations.
#[derive(Debug, Clone, PartialEq)]
pub enum ParseError {
    /// Invalid input at the given word position.
    InvalidInput(usize),

    /// Unterminated string starting at the given word position.
    UnterminatedString(usize),

    /// Unterminated comment starting at the given word position.
    UnterminatedComment(usize),

    /// An internal error occurred.
    ///
    /// This variant being returned signifies a bug in the parsing logic.
    Internal,
}

impl Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::InvalidInput(pos) => write!(f, "Invalid input at word position {}", pos),

            Self::UnterminatedString(pos) => {
                write!(f, "Unterminated string starting at word position {}", pos)
            }

            Self::UnterminatedComment(pos) => {
                write!(f, "Unterminated comment starting at word position {}", pos)
            }

            Self::Internal => write!(f, "An internal parser error occurred (this is a bug!)"),
        }
    }
}

impl core::convert::From<core::option::NoneError> for ParseError {
    fn from(_: core::option::NoneError) -> ParseError {
        ParseError::Internal
    }
}
