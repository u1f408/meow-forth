#![allow(unused_imports)]

use crate::alloc_prelude::*;
use crate::ast::{parser::AstParser, AstNode, AstNodeType};

#[test]
fn parse_empty() {
    let parser = AstParser::new();
    let res = parser.parse("").unwrap();
    assert_eq!(res.len(), 0);
}

#[test]
fn parse_single_word_with_surrounding_whitespace() {
    let parser = AstParser::new();
    let res = parser.parse("  hello  ").unwrap();

    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::Word);
    assert_eq!(res[0].node_name, "hello".to_string());
}

#[test]
fn parse_comment_standalone_is_ignored() {
    let parser = AstParser::new();
    let res = parser.parse("( comment )").unwrap();
    assert_eq!(res.len(), 0);
}

#[test]
fn parse_comment_surrounded_is_ignored() {
    let parser = AstParser::new();
    let res = parser.parse("before ( comment ) after ").unwrap();
    assert_eq!(res.len(), 2);
    assert_eq!(res[0].node_name, "before".to_string());
    assert_eq!(res[1].node_name, "after".to_string());
}

#[test]
fn parse_integer_positive_without_sign() {
    let parser = AstParser::new();
    let res = parser.parse("10").unwrap();
    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::Integer);
    assert_eq!(res[0].node_value, 10);
}

#[test]
fn parse_integer_positive_with_sign() {
    let parser = AstParser::new();
    let res = parser.parse("+10").unwrap();
    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::Integer);
    assert_eq!(res[0].node_value, 10);
}

#[test]
fn parse_integer_negative_with_sign() {
    let parser = AstParser::new();
    let res = parser.parse("-10").unwrap();
    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::Integer);
    assert_eq!(res[0].node_value, -10);
}

#[test]
fn parse_integer_hexadecimal_positive_without_sign() {
    let parser = AstParser::new();
    let res = parser.parse("Ah").unwrap();
    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::Integer);
    assert_eq!(res[0].node_value, 10);
}

#[test]
fn parse_integer_hexadecimal_positive_with_sign() {
    let parser = AstParser::new();
    let res = parser.parse("+Ah").unwrap();
    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::Integer);
    assert_eq!(res[0].node_value, 10);
}

#[test]
fn parse_integer_hexadecimal_negative_with_sign() {
    let parser = AstParser::new();
    let res = parser.parse("-Ah").unwrap();
    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::Integer);
    assert_eq!(res[0].node_value, -10);
}

#[test]
fn parse_string() {
    let parser = AstParser::new();
    let res = parser.parse(".\" Hello, world!\"").unwrap();
    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::String);
    assert_eq!(res[0].node_name, "Hello, world!");
}

#[test]
fn parse_string_with_escaped_quotes() {
    let parser = AstParser::new();
    let res = parser.parse(".\" Hello, \\\"world!\\\"\"").unwrap();
    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::String);
    assert_eq!(res[0].node_name, r#"Hello, "world!""#);
}

#[test]
fn parse_conditional() {
    let parser = AstParser::new();
    let res = parser.parse(r#"if ." true" else ." false" then"#).unwrap();

    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::Conditional);
    assert_eq!(res[0].children.len(), 2);
    assert_eq!(
        res[0].children[0].node_type,
        AstNodeType::ConditionalTrueBranch
    );
    assert_eq!(
        res[0].children[1].node_type,
        AstNodeType::ConditionalFalseBranch
    );
}

#[test]
fn parse_conditional_nested() {
    let parser = AstParser::new();
    let res = parser
        .parse(r#"if -1 if ." true" then else ." false" then"#)
        .unwrap();

    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::Conditional);
    assert_eq!(res[0].children.len(), 2);
    assert_eq!(
        res[0].children[0].node_type,
        AstNodeType::ConditionalTrueBranch
    );
    assert_eq!(res[0].children[0].children.len(), 2);
    assert_eq!(
        res[0].children[0].children[1].node_type,
        AstNodeType::Conditional
    );
}

#[test]
fn parse_func_def() {
    let parser = AstParser::new();
    let res = parser.parse(": name hello ;").unwrap();

    // Check the definition node
    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::FunctionDef);
    assert_eq!(res[0].node_value, 0);
    assert_eq!(res[0].node_name, "name".to_string());

    // Check children
    assert_eq!(res[0].children.len(), 1);
    assert_eq!(res[0].children[0].node_type, AstNodeType::Word);
    assert_eq!(res[0].children[0].node_name, "hello".to_string());
}

#[test]
fn parse_func_def_local() {
    let parser = AstParser::new();
    let res = parser.parse(":: name hello ;").unwrap();

    // Check the definition node
    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::FunctionDef);
    assert_eq!(res[0].node_value, 1);
    assert_eq!(res[0].node_name, "name".to_string());

    // Check children
    assert_eq!(res[0].children.len(), 1);
    assert_eq!(res[0].children[0].node_type, AstNodeType::Word);
    assert_eq!(res[0].children[0].node_name, "hello".to_string());
}

#[test]
fn parse_func_def_nested() {
    let parser = AstParser::new();
    let res = parser
        .parse(": outername hello : innername innerword ; hello ;")
        .unwrap();

    // Check the definition node
    assert_eq!(res.len(), 1);
    assert_eq!(res[0].node_type, AstNodeType::FunctionDef);
    assert_eq!(res[0].node_name, "outername".to_string());

    // Check we have the correct number of children
    assert_eq!(res[0].children.len(), 3);

    // Check the inner function definition node
    let inner_funcdef: AstNode = res[0].children[1].clone();
    assert_eq!(inner_funcdef.node_type, AstNodeType::FunctionDef);
    assert_eq!(inner_funcdef.node_name, "innername".to_string());
    assert_eq!(inner_funcdef.children.len(), 1);
}
