//! AST parsing logic.

use crate::alloc_prelude::*;
use crate::ast::{AstNode, ParseError};

pub mod basics;
pub mod blocks;
pub mod string;
#[cfg(test)]
mod tests;

/// AST node parser.
pub struct AstParser {
    /// Registered parser functions that this parser will use to parse input
    /// values.
    ///
    /// Parser functions take two arguments - a reference to the `AstParser`
    /// using the function, and a slice of words to attempt to parse. The
    /// function must return an `Ok` value containing a tuple of the number
    /// of words consumed by the parser, and the parsed [`AstNode`] entries;
    /// or an `Err(ParseError)`.
    pub parse_functions:
        Vec<&'static (dyn Fn(&AstParser, &[String]) -> Result<(usize, Vec<AstNode>), ParseError>)>,
}

impl AstParser {
    /// Creates an `AstParser` with no registered parser functions.
    pub fn new_without_parsers() -> Self {
        Self {
            parse_functions: Vec::new(),
        }
    }

    /// Creates an `AstParser` with the default set of parser functions.
    pub fn new() -> Self {
        let mut parser = Self::new_without_parsers();
        parser.parse_functions.push(&self::basics::parse_comment);
        parser.parse_functions.push(&self::basics::parse_integer);
        parser.parse_functions.push(&self::string::parse_string);
        parser
            .parse_functions
            .push(&self::blocks::parse_conditional);
        parser.parse_functions.push(&self::blocks::parse_function);
        parser.parse_functions.push(&self::blocks::parse_do_loop);
        parser.parse_functions.push(&self::blocks::parse_until_loop);

        parser
    }

    /// Parse a string as input words.
    ///
    /// Returns either an `Ok(Vec<AstNode>)` with the parsed AST nodes, or
    /// an `Err(ParseError)` with information about why the parse failed.
    pub fn parse(&self, input: &str) -> Result<Vec<AstNode>, ParseError> {
        // Split input on whitespace, throwing away any empty values
        let words = input
            .split(char::is_whitespace)
            .map(|x| x.to_string())
            .filter(|x| x.trim() != "")
            .collect::<Vec<String>>();

        // Bail early if we have nothing to parse
        if words.len() == 0 {
            return Ok(Vec::new());
        }

        let mut nodes: Vec<AstNode> = Vec::new();
        let mut idx: usize = 0;
        while idx < words.len() {
            let mut parser_idx: usize = 0;
            let mut parsed: bool = false;
            while !parsed && parser_idx < self.parse_functions.len() {
                let res = (self.parse_functions[parser_idx])(&self, &words[idx..words.len()]);

                // If we get an error, and that error has a position value,
                // adjust the position value to be absolute (the values we get
                // from individual parse functions are relative)
                if let Err(e) = res {
                    use ParseError::*;
                    let err = match e {
                        InvalidInput(pos) => InvalidInput(idx + pos),
                        UnterminatedString(pos) => UnterminatedString(idx + pos),
                        UnterminatedComment(pos) => UnterminatedComment(idx + pos),
                        _ => e,
                    };

                    return Err(err);
                }

                // If `skip` is non-zero, the parse function we called accepted
                // the current word(s), so skip ahead by the number of words
                // the parser consumed, and extend our nodes Vec with the
                // parser results
                let (skip, new_nodes) = res.unwrap();
                if skip > 0 {
                    parsed = true;
                    idx += skip;
                    if new_nodes.len() > 0 {
                        nodes.extend(new_nodes);
                    }
                }

                // Next parser!
                parser_idx += 1;
            }

            // If no parser accepted the current word, assume it's just a
            // plain word.
            if !parsed {
                nodes.push(AstNode::new_word(words[idx].clone()));
                idx += 1;
            }
        }

        Ok(nodes)
    }
}
