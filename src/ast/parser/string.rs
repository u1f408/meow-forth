//! AST parsing for string types.

use crate::alloc_prelude::*;
use crate::ast::{parser::AstParser, AstNode, ParseError};

/// Parse a string (`." [...] "`).
pub fn parse_string(
    _ast_parser: &AstParser,
    words: &[String],
) -> Result<(usize, Vec<AstNode>), ParseError> {
    if words[0] == ".\"" {
        let start_idx: usize = 1;
        let mut end_idx: usize = start_idx;
        let mut found: bool = false;

        while !found && end_idx < words.len() {
            if words[end_idx].ends_with("\"") && !words[end_idx].ends_with("\\\"") {
                found = true;
            }

            end_idx += 1;
        }

        if !found {
            return Err(ParseError::UnterminatedString(0));
        }

        // TODO: Implement proper escape sequences within strings
        let collected = words[start_idx..end_idx]
            .iter()
            .map(|x| x.replace("\\\"", "\""))
            .map(|x| x.to_string())
            .collect::<Vec<String>>()
            .join(" ")
            .strip_suffix("\"")?
            .to_string();

        let node = AstNode::new_string(collected);
        return Ok((end_idx, [node].to_vec()));
    }

    Ok((0, Vec::new()))
}
