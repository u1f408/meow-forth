//! AST parsing for basic types (such as integers).

use crate::alloc_prelude::*;
use crate::ast::{parser::AstParser, AstNode, ParseError};

/// Parse a comment (`( [...] )`), ignoring the contents.
pub fn parse_comment(
    _ast_parser: &AstParser,
    words: &[String],
) -> Result<(usize, Vec<AstNode>), ParseError> {
    let mut idx: usize = 0;
    if words[idx] == "(" {
        let mut found: bool = false;
        while !found && idx < words.len() {
            if words[idx] == ")" {
                found = true;
            }

            idx += 1;
        }

        if !found {
            return Err(ParseError::UnterminatedComment(idx));
        }

        return Ok((idx, Vec::new()));
    }

    Ok((0, Vec::new()))
}

/// Parse an integer (base 10: `10`, base 16: `Ah`).
pub fn parse_integer(
    _ast_parser: &AstParser,
    words: &[String],
) -> Result<(usize, Vec<AstNode>), ParseError> {
    // Can we parse this as a base 10 integer?
    if let Ok(i) = isize::from_str_radix(&words[0], 10) {
        return Ok((1, [AstNode::new_integer(i)].to_vec()));
    }

    // Can we parse this as a base 16 integer (with a `h` suffix)?
    if let Some(s) = words[0].strip_suffix("h") {
        if let Ok(i) = isize::from_str_radix(s, 16) {
            return Ok((1, [AstNode::new_integer(i)].to_vec()));
        }
    }

    Ok((0, Vec::new()))
}
