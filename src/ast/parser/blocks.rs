//! AST parsing for block types (conditionals, loops, and functions).

use crate::alloc_prelude::*;
use crate::ast::{parser::AstParser, AstNode, ParseError};

/// Parse a conditional block (`if [...] else [...] then`).
pub fn parse_conditional(
    ast_parser: &AstParser,
    words: &[String],
) -> Result<(usize, Vec<AstNode>), ParseError> {
    if words[0] == "if" {
        let mut end_idx: usize = 1;
        let mut nested_if: usize = 0;
        let mut found_else: bool = false;
        let mut found_then: bool = false;

        let mut if_true: Vec<String> = Vec::new();
        let mut if_false: Vec<String> = Vec::new();

        while !found_then && end_idx < words.len() {
            let mut push_word: bool = false;

            if words[end_idx] == "if" {
                nested_if += 1;
                push_word = true;
            } else if words[end_idx] == "then" {
                if nested_if > 0 {
                    nested_if -= 1;
                    push_word = true;
                } else {
                    found_then = true;
                }
            } else if words[end_idx] == "else" {
                if nested_if > 0 {
                    push_word = true;
                } else {
                    found_else = true;
                }
            } else {
                push_word = true;
            }

            if push_word {
                if !found_else {
                    if_true.push(words[end_idx].clone());
                } else {
                    if_false.push(words[end_idx].clone());
                }
            }

            end_idx += 1;
        }

        if !found_then {
            return Err(ParseError::InvalidInput(end_idx));
        }

        let parsed_true = ast_parser.parse(&if_true.join(" "))?;
        let parsed_false = ast_parser.parse(&if_false.join(" "))?;

        let node = AstNode::new_conditional(parsed_true, parsed_false);
        return Ok((end_idx, [node].to_vec()));
    }

    Ok((0, Vec::new()))
}

/// Parse a function definition (`: <name> [...] ;` or `:: <name> [...] ;`)
///
/// Starting the function with a single colon (`:`) stores the function in
/// the global scope, and starting the function with a double colon (`::`)
/// stores the function in the local scope.
pub fn parse_function(
    ast_parser: &AstParser,
    words: &[String],
) -> Result<(usize, Vec<AstNode>), ParseError> {
    // A single colon means "global scope", a double-colon means "local scope"
    if words[0] == ":" || words[0] == "::" {
        let mut end_idx: usize = 1;
        let mut nested: usize = 0;
        let mut found: bool = false;

        while !found && end_idx < words.len() {
            if words[end_idx] == ":" || words[end_idx] == "::" {
                nested += 1;
            } else if words[end_idx] == ";" {
                if nested > 0 {
                    nested -= 1;
                } else {
                    found = true;
                }
            }

            end_idx += 1;
        }

        if !found {
            return Err(ParseError::InvalidInput(end_idx));
        }

        let inner: String = words[2..(end_idx - 1)]
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>()
            .join(" ");

        let parsed = ast_parser.parse(&inner)?;
        let node = AstNode::new_function(words[0] == ":", &words[1], parsed);
        return Ok((end_idx, [node].to_vec()));
    }

    Ok((0, Vec::new()))
}

/// Parse a `do [...] loop` loop.
pub fn parse_do_loop(
    ast_parser: &AstParser,
    words: &[String],
) -> Result<(usize, Vec<AstNode>), ParseError> {
    if words[0] == "do" {
        let mut end_idx: usize = 1;
        let mut nested: usize = 0;
        let mut found: bool = false;

        while !found && end_idx < words.len() {
            if words[end_idx] == "do" {
                nested += 1;
            } else if words[end_idx] == "loop" {
                if nested > 0 {
                    nested -= 1;
                } else {
                    found = true;
                }
            }

            end_idx += 1;
        }

        if !found {
            return Err(ParseError::InvalidInput(end_idx));
        }

        let inner: String = words[1..(end_idx - 1)]
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>()
            .join(" ");

        let parsed = ast_parser.parse(&inner)?;
        let node = AstNode::new_do_loop(parsed);
        return Ok((end_idx, [node].to_vec()));
    }

    Ok((0, Vec::new()))
}

/// Parse a `begin [...] until` loop.
pub fn parse_until_loop(
    ast_parser: &AstParser,
    words: &[String],
) -> Result<(usize, Vec<AstNode>), ParseError> {
    if words[0] == "begin" {
        let mut end_idx: usize = 1;
        let mut nested: usize = 0;
        let mut found: bool = false;

        while !found && end_idx < words.len() {
            if words[end_idx] == "begin" {
                nested += 1;
            } else if words[end_idx] == "until" {
                if nested > 0 {
                    nested -= 1;
                } else {
                    found = true;
                }
            }

            end_idx += 1;
        }

        if !found {
            return Err(ParseError::InvalidInput(end_idx));
        }

        let inner: String = words[1..(end_idx - 1)]
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>()
            .join(" ");

        let parsed = ast_parser.parse(&inner)?;
        let node = AstNode::new_until_loop(parsed);
        return Ok((end_idx, [node].to_vec()));
    }

    Ok((0, Vec::new()))
}
