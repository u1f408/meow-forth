//! Encode and decode AST nodes using B3.

use crate::alloc_prelude::*;
use crate::ast::{AstNode, AstNodeType};
use core::convert::TryInto;

#[derive(Debug, Clone, PartialEq)]
pub enum AstB3Error {
    B3Error(b3_rs::Error),
    IncorrectTypeError,
    InvalidAstItem,
}

impl From<core::option::NoneError> for AstB3Error {
    fn from(_: core::option::NoneError) -> Self {
        Self::IncorrectTypeError
    }
}

impl From<core::num::TryFromIntError> for AstB3Error {
    fn from(_: core::num::TryFromIntError) -> Self {
        Self::IncorrectTypeError
    }
}

/// Construct a B3 CompositeList from the given list of `AstNode` objects,
/// including their children.
///
/// The output of this function is suitable for encoding to a B3 byte
/// representation, the output of this function can be passed to the
/// [`b3_item_to_ast_list`] function to reconstruct a full AST.
pub fn ast_list_to_b3_item(nodes: &[AstNode]) -> Result<b3_rs::Item, AstB3Error> {
    let mut list: Vec<b3_rs::Item> = Vec::new();
    for item in nodes.iter() {
        list.push(ast_to_b3_item(item)?);
    }

    let clist = b3_rs::ItemValue::CompositeList(list);
    Ok(b3_rs::Item::new(b3_rs::ItemKey::NoKey, clist))
}

/// Recursively construct B3 items from the given `AstNode`, including it's
/// children.
///
/// This only handles a singular `AstNode` and it's children. For a list of
/// `AstNode` objects, as you would give to the interpreter, use the
/// [`ast_list_to_b3_item`] function.
pub fn ast_to_b3_item(node: &AstNode) -> Result<b3_rs::Item, AstB3Error> {
    // Node type
    let n_type = b3_rs::Item::from(node.node_type.clone() as u64).with_key(b3_rs::ItemKey::from(1));

    // Node name
    let n_name = b3_rs::Item::from(node.node_name.clone()).with_key(b3_rs::ItemKey::from(2));

    // Node value
    let n_value = b3_rs::Item::from(node.node_value as i64).with_key(b3_rs::ItemKey::from(3));

    // Children
    let n_children = {
        let mut v: Vec<b3_rs::Item> = Vec::new();
        for child in node.children.iter() {
            v.push(ast_to_b3_item(child)?);
        }

        let dv = b3_rs::ItemValue::CompositeList(v);
        b3_rs::Item::new(b3_rs::ItemKey::from(4), dv)
    };

    // Construct dictionary
    let dict = b3_rs::ItemValue::CompositeDict(vec![n_type, n_name, n_value, n_children]);

    Ok(b3_rs::Item::new(b3_rs::ItemKey::NoKey, dict))
}

/// Recursively construct a list of `AstNode` objects from the given B3
/// `CompositeList` item.
///
/// This function is suitable for parsing a decoded B3 byte representation,
/// as it would be returned by [`ast_list_to_b3_item`], and can be used for
/// encoding a full AST.
pub fn b3_item_to_ast_list(i: b3_rs::Item) -> Result<Vec<AstNode>, AstB3Error> {
    let mut out: Vec<AstNode> = Vec::new();
    let entries = i.value.get_entries()?;
    for item in entries {
        out.push(b3_item_to_ast(item)?);
    }

    Ok(out)
}

/// Recursively construct `AstNode` objects from the given B3 item.
///
/// This only handles a singular `AstNode` and it's children. For a list of
/// `AstNode` objects, as you would give to the interpreter, use the
/// [`b3_item_to_ast_list`] function.
pub fn b3_item_to_ast(i: b3_rs::Item) -> Result<AstNode, AstB3Error> {
    let entries = i.value.get_entries()?;

    // Create a null item, and clone it to our needed parameters - this is so
    // we can easily null-item-check these further down and return an error
    // if we're missing something
    let null = b3_rs::Item::new(b3_rs::ItemKey::NoKey, b3_rs::ItemValue::Null);
    let mut n_type = null.clone();
    let mut n_name = null.clone();
    let mut n_value = null.clone();
    let mut n_children = null.clone();

    // Iterate over entries in this B3 item, matching on keys to populate
    // the above variables
    for e in entries.iter() {
        if let b3_rs::ItemKey::IntegerKey(i) = e.key {
            match i {
                1 => {
                    n_type = e.clone();
                }

                2 => {
                    n_name = e.clone();
                }

                3 => {
                    n_value = e.clone();
                }

                4 => {
                    n_children = e.clone();
                }

                _ => {}
            }
        }
    }

    // Null check our variables, returning an error if something's not there
    if n_type == null || n_name == null || n_value == null || n_children == null {
        return Err(AstB3Error::InvalidAstItem);
    }

    // Get node type
    let node_type = AstNodeType::from(n_type.value.get_u64()?);

    // Recursively parse children
    let children = {
        let mut v: Vec<AstNode> = Vec::new();
        if let b3_rs::ItemValue::CompositeList(ncv) = n_children.value {
            for child in ncv.iter() {
                v.push(b3_item_to_ast(child.clone())?);
            }
        }

        v
    };

    // Create AstNode
    let node = AstNode {
        node_type,
        node_name: n_name.value.get_utf8()?,
        node_value: n_value.value.get_i64()?.try_into()?,
        children,
    };

    Ok(node)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn single_ast_node_to_b3_item() {
        let node = AstNode::new_word(String::from("test"));
        let item = ast_to_b3_item(&node).unwrap();

        let entries = item.value.get_entries().unwrap();
        assert_eq!(entries.len(), 4);
        assert_eq!(entries[0].key, b3_rs::ItemKey::from(1));
        assert_eq!(entries[1].key, b3_rs::ItemKey::from(2));
        assert_eq!(entries[2].key, b3_rs::ItemKey::from(3));
        assert_eq!(entries[3].key, b3_rs::ItemKey::from(4));

        let children = entries[3].value.get_entries().unwrap();
        assert_eq!(children.len(), 0);
    }

    #[test]
    fn recursive_ast_node_to_b3_item() {
        let inner_node = AstNode::new_word(String::from("test"));
        let outer_node = AstNode::new_function(true, "fn", vec![inner_node]);
        let item = ast_to_b3_item(&outer_node).unwrap();

        let entries = item.value.get_entries().unwrap();
        assert_eq!(entries.len(), 4);
        assert_eq!(entries[0].key, b3_rs::ItemKey::from(1));
        assert_eq!(entries[1].key, b3_rs::ItemKey::from(2));
        assert_eq!(entries[2].key, b3_rs::ItemKey::from(3));
        assert_eq!(entries[3].key, b3_rs::ItemKey::from(4));

        let children = entries[3].value.get_entries().unwrap();
        assert_eq!(children.len(), 1);

        let child_entries = children[0].value.get_entries().unwrap();
        assert_eq!(child_entries.len(), 4);
        assert_eq!(child_entries[0].key, b3_rs::ItemKey::from(1));
        assert_eq!(child_entries[1].key, b3_rs::ItemKey::from(2));
        assert_eq!(child_entries[2].key, b3_rs::ItemKey::from(3));
        assert_eq!(child_entries[3].key, b3_rs::ItemKey::from(4));
    }

    #[test]
    fn single_b3_item_to_ast_node() {
        let i_type = b3_rs::Item::from(1u64).with_key(b3_rs::ItemKey::from(1));
        let i_name = b3_rs::Item::from("test").with_key(b3_rs::ItemKey::from(2));
        let i_value = b3_rs::Item::from(0i64).with_key(b3_rs::ItemKey::from(3));
        let i_children = b3_rs::Item::new(
            b3_rs::ItemKey::from(4),
            b3_rs::ItemValue::CompositeList(vec![]),
        );
        let ctr_vec = vec![i_type, i_name, i_value, i_children];
        let ctr = b3_rs::Item::new(
            b3_rs::ItemKey::NoKey,
            b3_rs::ItemValue::CompositeDict(ctr_vec),
        );

        let ast_node = b3_item_to_ast(ctr).unwrap();
        assert_eq!(ast_node.node_type, AstNodeType::Word);
        assert_eq!(ast_node.node_name, String::from("test"));
        assert_eq!(ast_node.node_value, 0);
        assert_eq!(ast_node.children.len(), 0);
    }

    #[test]
    fn recursive_b3_item_to_ast_node() {
        let i_type = b3_rs::Item::from(1u64).with_key(b3_rs::ItemKey::from(1));
        let i_name = b3_rs::Item::from("test").with_key(b3_rs::ItemKey::from(2));
        let i_value = b3_rs::Item::from(0i64).with_key(b3_rs::ItemKey::from(3));
        let i_children_i = b3_rs::ItemValue::CompositeList(vec![]);
        let i_children = b3_rs::Item::new(b3_rs::ItemKey::from(4), i_children_i);
        let i_ctr_vec = vec![i_type, i_name, i_value, i_children];
        let i_ctr = b3_rs::Item::new(
            b3_rs::ItemKey::NoKey,
            b3_rs::ItemValue::CompositeDict(i_ctr_vec),
        );
        let o_type = b3_rs::Item::from(4u64).with_key(b3_rs::ItemKey::from(1));
        let o_name = b3_rs::Item::from("fn").with_key(b3_rs::ItemKey::from(2));
        let o_value = b3_rs::Item::from(0i64).with_key(b3_rs::ItemKey::from(3));
        let o_children_i = b3_rs::ItemValue::CompositeList(vec![i_ctr]);
        let o_children = b3_rs::Item::new(b3_rs::ItemKey::from(4), o_children_i);
        let o_ctr_vec = vec![o_type, o_name, o_value, o_children];
        let o_ctr = b3_rs::Item::new(
            b3_rs::ItemKey::NoKey,
            b3_rs::ItemValue::CompositeDict(o_ctr_vec),
        );

        let ast_node = b3_item_to_ast(o_ctr).unwrap();
        assert_eq!(ast_node.node_type, AstNodeType::FunctionDef);
        assert_eq!(ast_node.node_name, String::from("fn"));
        assert_eq!(ast_node.node_value, 0);
        assert_eq!(ast_node.children.len(), 1);
        assert_eq!(ast_node.children[0].node_type, AstNodeType::Word);
        assert_eq!(ast_node.children[0].node_name, String::from("test"));
        assert_eq!(ast_node.children[0].node_value, 0);
        assert_eq!(ast_node.children[0].children.len(), 0);
    }
}
