//! Built-in category "debug" - Debugging utilities
//!
//! Built-ins in this category are not automatically loaded on interpreter
//! creation,. Use the `i_add_debug` function to add debugging functions
//! to an interpreter.

#![allow(unused_imports)]

use crate::alloc_prelude::*;
use crate::ast::b3::ast_list_to_b3_item;
use crate::ast::{parser::AstParser, AstNode, AstNodeType};
use crate::interpreter::{Interpreter, InterpreterError, InterpreterNativeData};

/// Add functions from the "debug" category to the given interpreter.
pub fn i_add_debug(i: &mut Interpreter) {
    // Add native function definitions
    i.native_defs
        .push((String::from("debug-dump-ast!"), Box::new(&n_dump_ast)));
    i.native_defs
        .push((String::from("debug-encode-ast!"), Box::new(&n_encode_ast)));
    i.native_defs
        .push((String::from("debug-dump-fns!"), Box::new(&n_dump_fns)));
    i.native_defs
        .push((String::from("debug-encode-fns!"), Box::new(&n_encode_fns)));
}

/// Native implementation of the `debug-dump-ast!` word.
///
/// This uses the value on the top of the stack as the number of nodes to
/// dump. The nodes to dump must follow the call to this function. This dumps
/// the nodes to the output, formatted as `{:#?}`.
pub fn n_dump_ast(mut d: InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError> {
    if let Some(count) = d.stack.pop() {
        let count = count as usize;
        if d.nodes.len() < count + 1 {
            return Err(InterpreterError::Internal);
        }

        d.output = format!("{:#?}", &d.nodes[1..(count + 1)]);
        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

/// Native implementation of the `debug-encode-ast!` word.
///
/// This uses the value on the top of the stack as the number of nodes to
/// encode to a B3 byte representation. The nodes to encode must follow the
/// call to this function.
pub fn n_encode_ast(
    mut d: InterpreterNativeData,
) -> Result<InterpreterNativeData, InterpreterError> {
    if let Some(count) = d.stack.pop() {
        let count = count as usize;
        if d.nodes.len() < count + 1 {
            return Err(InterpreterError::Internal);
        }

        d.output = match ast_list_to_b3_item(&d.nodes[1..(count + 1)]) {
            Ok(item) => {
                // Encode to byte array
                let data = item.encode().or(Err(InterpreterError::Internal))?;

                // Encode byte array to hex
                hex::encode(data)
            }

            Err(_) => {
                return Err(InterpreterError::Internal);
            }
        };

        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

/// Native implementation of the `debug-dump-fns!` word.
///
/// This dumps all Forth-language functions in the interpreter's
/// `function_defs` array to the output, formatted as `{:#?}`.
pub fn n_dump_fns(mut d: InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError> {
    d.output = format!("{:#?}", &d.function_defs);
    Ok(d)
}

/// Native implementation of the `debug-encode-fns!` word.
///
/// This encodes all Forth-language functions in the interpreter's
/// `function_defs` array to a B3 byte representation.
pub fn n_encode_fns(
    mut d: InterpreterNativeData,
) -> Result<InterpreterNativeData, InterpreterError> {
    d.output = match ast_list_to_b3_item(&d.function_defs) {
        Ok(item) => {
            // Encode to byte array
            let data = item.encode().or(Err(InterpreterError::Internal))?;

            // Encode byte array to hex
            hex::encode(data)
        }

        Err(_) => {
            return Err(InterpreterError::Internal);
        }
    };

    Ok(d)
}
