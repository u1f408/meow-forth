//! Built-in function definitions, both native and parsed Forth.
//!
//! Function names in this module, and it's submodules, follow the following
//! format:
//!
//! - Functions with names starting with `n_` are native functions, to be added
//!   to the `native_defs` list on the interpreter.
//! - Functions with names starting with `p_` are parsed Forth functions, the
//!   **return value** of which are added to the `function_defs` list on the
//!   interpreter.
//! - Functions with names starting with `i_` are helper functions that take an
//!   interpreter instance and add the functions in their category to that
//!   interpreter.

pub mod basics;
pub mod debug;
pub mod print;
pub mod stack;

/// Add built-in functions to the given interpreter.
///
/// This adds all functions in the modules:
///
/// - `basics`
/// - `print`
/// - `stack`
pub fn i_add(i: &mut crate::Interpreter) {
    self::basics::i_add_basics(i);
    self::print::i_add_print(i);
    self::stack::i_add_stack(i);
}
