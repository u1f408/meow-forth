//! Built-in category "basics"

#![allow(unused_imports)]

use crate::alloc_prelude::*;
use crate::ast::{parser::AstParser, AstNode, AstNodeType};
use crate::interpreter::{Interpreter, InterpreterError, InterpreterNativeData};

/// Add functions from the "basics" category to the given interpreter.
pub fn i_add_basics(i: &mut Interpreter) {
    // Add native function definitions
    i.native_defs.push((String::from("="), Box::new(&n_equal)));
    i.native_defs
        .push((String::from(">"), Box::new(&n_greater_than)));
    i.native_defs
        .push((String::from("<"), Box::new(&n_less_than)));
    i.native_defs.push((String::from("and"), Box::new(&n_and)));
    i.native_defs.push((String::from("or"), Box::new(&n_or)));
    i.native_defs
        .push((String::from("invert"), Box::new(&n_invert)));
}

/// Native implementation of the `=` word.
pub fn n_equal(mut d: InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError> {
    if let (Some(one), Some(two)) = (d.stack.pop(), d.stack.pop()) {
        if two == one {
            d.stack.push(-1);
        } else {
            d.stack.push(0);
        }

        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

/// Native implementation of the `>` word.
pub fn n_greater_than(
    mut d: InterpreterNativeData,
) -> Result<InterpreterNativeData, InterpreterError> {
    if let (Some(one), Some(two)) = (d.stack.pop(), d.stack.pop()) {
        if two > one {
            d.stack.push(-1);
        } else {
            d.stack.push(0);
        }

        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

/// Native implementation of the `<` word.
pub fn n_less_than(
    mut d: InterpreterNativeData,
) -> Result<InterpreterNativeData, InterpreterError> {
    if let (Some(one), Some(two)) = (d.stack.pop(), d.stack.pop()) {
        if two < one {
            d.stack.push(-1);
        } else {
            d.stack.push(0);
        }

        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

/// Native implementation of the `and` word.
pub fn n_and(mut d: InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError> {
    if let (Some(one), Some(two)) = (d.stack.pop(), d.stack.pop()) {
        d.stack.push(two & one);
        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

/// Native implementation of the `or` word.
pub fn n_or(mut d: InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError> {
    if let (Some(one), Some(two)) = (d.stack.pop(), d.stack.pop()) {
        d.stack.push(two | one);
        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

/// Native implementation of the `invert` word.
pub fn n_invert(mut d: InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError> {
    if let Some(v) = d.stack.pop() {
        d.stack.push(!v);
        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::ast::{AstNode, AstNodeType};

    #[test]
    fn equal() {
        let mut i = Interpreter::new_empty();
        i.native_defs.push((String::from("="), Box::new(&n_equal)));

        let res = i.execute(vec![
            AstNode::new_integer(1),
            AstNode::new_integer(1),
            AstNode::new_word(String::from("=")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack.len(), 1);
        assert_eq!(i.stack.pop(), Some(-1));

        let res = i.execute(vec![
            AstNode::new_integer(1),
            AstNode::new_integer(2),
            AstNode::new_word(String::from("=")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack.len(), 1);
        assert_eq!(i.stack.pop(), Some(0));
    }

    #[test]
    fn greater_than() {
        let mut i = Interpreter::new_empty();
        i.native_defs
            .push((String::from(">"), Box::new(&n_greater_than)));

        let res = i.execute(vec![
            AstNode::new_integer(1),
            AstNode::new_integer(2),
            AstNode::new_word(String::from(">")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack.len(), 1);
        assert_eq!(i.stack.pop(), Some(0));

        let res = i.execute(vec![
            AstNode::new_integer(2),
            AstNode::new_integer(1),
            AstNode::new_word(String::from(">")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack.len(), 1);
        assert_eq!(i.stack.pop(), Some(-1));
    }

    #[test]
    fn less_than() {
        let mut i = Interpreter::new_empty();
        i.native_defs
            .push((String::from("<"), Box::new(&n_less_than)));

        let res = i.execute(vec![
            AstNode::new_integer(1),
            AstNode::new_integer(2),
            AstNode::new_word(String::from("<")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack.len(), 1);
        assert_eq!(i.stack.pop(), Some(-1));

        let res = i.execute(vec![
            AstNode::new_integer(2),
            AstNode::new_integer(1),
            AstNode::new_word(String::from("<")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack.len(), 1);
        assert_eq!(i.stack.pop(), Some(0));
    }

    #[test]
    fn and() {
        let mut i = Interpreter::new_empty();
        i.native_defs.push((String::from("and"), Box::new(&n_and)));

        let res = i.execute(vec![
            AstNode::new_integer(0x0123),
            AstNode::new_integer(0x00FF),
            AstNode::new_word(String::from("and")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack.len(), 1);
        assert_eq!(i.stack.pop(), Some(0x23));
    }

    #[test]
    fn or() {
        let mut i = Interpreter::new_empty();
        i.native_defs.push((String::from("or"), Box::new(&n_or)));

        let res = i.execute(vec![
            AstNode::new_integer(0x1234),
            AstNode::new_integer(0x00FF),
            AstNode::new_word(String::from("or")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack.len(), 1);
        assert_eq!(i.stack.pop(), Some(0x12FF));
    }

    #[test]
    fn invert() {
        let mut i = Interpreter::new_empty();
        i.native_defs
            .push((String::from("invert"), Box::new(&n_invert)));

        let res = i.execute(vec![
            AstNode::new_integer(-1),
            AstNode::new_word(String::from("invert")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack.len(), 1);
        assert_eq!(i.stack.pop(), Some(0));

        let res = i.execute(vec![
            AstNode::new_integer(0),
            AstNode::new_word(String::from("invert")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack.len(), 1);
        assert_eq!(i.stack.pop(), Some(-1));
    }
}
