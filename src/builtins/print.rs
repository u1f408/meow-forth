//! Built-in category "print" - printing things

#![allow(unused_imports)]

use crate::alloc_prelude::*;
use crate::ast::{parser::AstParser, AstNode, AstNodeType};
use crate::interpreter::{Interpreter, InterpreterError, InterpreterNativeData};

/// Add functions from the "print" category to the given interpreter.
pub fn i_add_print(i: &mut Interpreter) {
    // Add native function definitions
    i.native_defs.push((String::from("."), Box::new(&n_dot)));
    i.native_defs
        .push((String::from("emit"), Box::new(&n_emit)));

    // Add Forth function definitions
    i.function_defs.extend(p_cr());
}

/// Native implementation of the `.` word.
pub fn n_dot(mut d: InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError> {
    if let Some(c) = d.stack.pop() {
        d.output = format!("{}", c);

        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

/// Native implementation of the `emit` word.
pub fn n_emit(mut d: InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError> {
    if let Some(c) = d.stack.pop() {
        let ch = (c & 0xFF) as u8 as char;
        d.output = format!("{}", ch);

        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

/// Parsed Forth implementation of the `cr` word.
pub fn p_cr() -> Vec<AstNode> {
    let parser = AstParser::new();
    parser.parse(": cr ( -- ) 10 emit ;").unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::ast::{AstNode, AstNodeType};

    #[test]
    fn dot() {
        let mut i = Interpreter::new_empty();
        i.native_defs.push((String::from("."), Box::new(&n_dot)));

        let res = i.execute(vec![
            AstNode::new_integer(1),
            AstNode::new_word(String::from(".")),
        ]);

        assert!(res.is_ok());
        assert_eq!(res.unwrap(), vec![String::from("1")]);
        assert_eq!(i.stack.len(), 0);
    }

    #[test]
    fn emit() {
        let mut i = Interpreter::new_empty();
        i.native_defs
            .push((String::from("emit"), Box::new(&n_emit)));

        let res = i.execute(vec![
            AstNode::new_integer(0x30),
            AstNode::new_word(String::from("emit")),
        ]);

        assert!(res.is_ok());
        assert_eq!(res.unwrap(), vec![String::from("0")]);
        assert_eq!(i.stack.len(), 0);
    }

    #[test]
    fn cr() {
        let mut i = Interpreter::new_empty();
        i.native_defs
            .push((String::from("emit"), Box::new(&n_emit)));
        i.function_defs.extend(p_cr());

        let res = i.execute(vec![AstNode::new_word(String::from("cr"))]);

        assert!(res.is_ok());
        assert_eq!(res.unwrap(), vec![String::from("\n")]);
    }
}
