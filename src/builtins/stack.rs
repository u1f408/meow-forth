//! Built-in category "stack" - stack manipulation

#![allow(unused_imports)]

use crate::alloc_prelude::*;
use crate::ast::{parser::AstParser, AstNode, AstNodeType};
use crate::interpreter::{Interpreter, InterpreterError, InterpreterNativeData};

/// Add functions from the "stack" category to the given interpreter.
pub fn i_add_stack(i: &mut Interpreter) {
    // Add native function definitions
    i.native_defs
        .push((String::from("drop"), Box::new(&n_drop)));
    i.native_defs.push((String::from("dup"), Box::new(&n_dup)));
    i.native_defs
        .push((String::from("swap"), Box::new(&n_swap)));
    i.native_defs
        .push((String::from("over"), Box::new(&n_over)));
    i.native_defs.push((String::from("rot"), Box::new(&n_rot)));
}

/// Native implementation of the `drop` word.
pub fn n_drop(mut d: InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError> {
    if let Some(_) = d.stack.pop() {
        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

/// Native implementation of the `dup` word.
pub fn n_dup(mut d: InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError> {
    if let Some(c) = d.stack.pop() {
        d.stack.push(c);
        d.stack.push(c);

        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

/// Native implementation of the `swap` word.
pub fn n_swap(mut d: InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError> {
    if let (Some(one), Some(two)) = (d.stack.pop(), d.stack.pop()) {
        d.stack.push(one);
        d.stack.push(two);

        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

/// Native implementation of the `over` word.
pub fn n_over(mut d: InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError> {
    if let (Some(one), Some(two)) = (d.stack.pop(), d.stack.pop()) {
        d.stack.push(two);
        d.stack.push(one);
        d.stack.push(two);

        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

/// Native implementation of the `rot` word.
pub fn n_rot(mut d: InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError> {
    if let (Some(one), Some(two), Some(three)) = (d.stack.pop(), d.stack.pop(), d.stack.pop()) {
        d.stack.push(two);
        d.stack.push(one);
        d.stack.push(three);

        Ok(d)
    } else {
        Err(InterpreterError::StackUnderflow)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::ast::{AstNode, AstNodeType};

    #[test]
    fn drop() {
        let mut i = Interpreter::new_empty();
        i.native_defs
            .push((String::from("drop"), Box::new(&n_drop)));

        let res = i.execute(vec![
            AstNode::new_integer(1),
            AstNode::new_integer(2),
            AstNode::new_word(String::from("drop")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack, vec![1]);
    }

    #[test]
    fn dup() {
        let mut i = Interpreter::new_empty();
        i.native_defs.push((String::from("dup"), Box::new(&n_dup)));

        let res = i.execute(vec![
            AstNode::new_integer(1),
            AstNode::new_integer(2),
            AstNode::new_word(String::from("dup")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack, vec![1, 2, 2]);
    }

    #[test]
    fn swap() {
        let mut i = Interpreter::new_empty();
        i.native_defs
            .push((String::from("swap"), Box::new(&n_swap)));

        let res = i.execute(vec![
            AstNode::new_integer(1),
            AstNode::new_integer(2),
            AstNode::new_integer(3),
            AstNode::new_integer(4),
            AstNode::new_word(String::from("swap")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack, vec![1, 2, 4, 3]);
    }

    #[test]
    fn over() {
        let mut i = Interpreter::new_empty();
        i.native_defs
            .push((String::from("over"), Box::new(&n_over)));

        let res = i.execute(vec![
            AstNode::new_integer(1),
            AstNode::new_integer(2),
            AstNode::new_integer(3),
            AstNode::new_word(String::from("over")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack, vec![1, 2, 3, 2]);
    }

    #[test]
    fn rot() {
        let mut i = Interpreter::new_empty();
        i.native_defs.push((String::from("rot"), Box::new(&n_rot)));

        let res = i.execute(vec![
            AstNode::new_integer(1),
            AstNode::new_integer(2),
            AstNode::new_integer(3),
            AstNode::new_word(String::from("rot")),
        ]);

        assert!(res.is_ok());
        assert_eq!(i.stack, vec![2, 3, 1]);
    }
}
