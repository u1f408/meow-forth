#![cfg_attr(not(feature = "std"), no_std)]
#![feature(alloc_prelude)]
#![feature(str_strip)]
#![feature(try_trait)]

#[cfg(feature = "std")]
#[macro_use]
extern crate std;
#[cfg(feature = "std")]
use std::prelude::v1 as alloc_prelude;
#[cfg(not(feature = "std"))]
#[macro_use]
extern crate alloc;
#[cfg(not(feature = "std"))]
use alloc::prelude::v1 as alloc_prelude;

pub mod ast;
pub mod builtins;
mod interpreter;
pub use self::interpreter::*;
