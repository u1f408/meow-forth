//! The meow_forth interpreter.

use crate::alloc_prelude::*;
use crate::ast::{AstNode, AstNodeType};
use core::fmt::{self, Debug};

mod error;
#[cfg(test)]
mod tests;
pub use self::error::InterpreterError;

/// Values passed in to, and out of, a native function.
#[derive(Debug, Clone)]
pub struct InterpreterNativeData {
    /// Interpreter stack.
    pub stack: Vec<isize>,

    /// List of AST nodes that the interpreter will execute, starting at the
    /// node being executed at the point of the call.
    pub nodes: Vec<AstNode>,

    /// Forth-language function definitions known by the interpreter.
    pub function_defs: Vec<AstNode>,

    /// The number of AST nodes to skip over after the end of this call.
    pub skip: usize,

    /// The output of the execution, as a string.
    pub output: String,
}

/// The core meow_forth interpreter.
pub struct Interpreter {
    /// Interpreter stack.
    pub stack: Vec<isize>,

    /// List of parsed global-scope Forth function definitions.
    ///
    /// Entries in this list are expected to have a type of `FunctionDef`,
    /// which is enforced by [`add_function`](#method.add_function).
    /// Interpreter behaviour is undefined should an entry in this list not
    /// have a type of `FunctionDef`.
    pub function_defs: Vec<AstNode>,

    /// List of native function definitions.
    ///
    /// The first element in the tuple is the name of the function, and the
    /// second element is the function to call.
    pub native_defs: Vec<(
        String,
        Box<dyn (Fn(InterpreterNativeData) -> Result<InterpreterNativeData, InterpreterError>)>,
    )>,
}

impl Interpreter {
    /// Creates an empty interpreter (that is, with no function definitions).
    pub fn new_empty() -> Self {
        Self {
            stack: Vec::new(),
            function_defs: Vec::new(),
            native_defs: Vec::new(),
        }
    }

    /// Creates an interpreter with built-in functions registered.
    pub fn new() -> Self {
        let mut i = Self::new_empty();
        crate::builtins::i_add(&mut i);

        i
    }

    /// Add a Forth function to the interpreter's global function list.
    ///
    /// Returns an error if the passed `AstNode` is not a `FunctionDef`.
    pub fn add_function(&mut self, node: AstNode) -> Result<(), InterpreterError> {
        if node.node_type != AstNodeType::FunctionDef {
            return Err(InterpreterError::InvalidNodeType(
                String::from("Interpreter.add_function"),
                node.node_type,
            ));
        }

        self.function_defs.push(node);
        Ok(())
    }

    /// Executes the input `AstNode` list.
    pub fn execute(&mut self, input: Vec<AstNode>) -> Result<Vec<String>, InterpreterError> {
        let res = self.execute_scoped(input, Vec::new());
        if let Err(e) = res {
            return Err(e);
        }

        let out = res.unwrap();
        Ok(out)
    }

    /// Executes the input `AstNode` list, additionally accepting a list of
    /// local-scope function definitions.
    pub fn execute_scoped(
        &mut self,
        input: Vec<AstNode>,
        fn_scope: Vec<AstNode>,
    ) -> Result<Vec<String>, InterpreterError> {
        let mut fn_scope: Vec<AstNode> = fn_scope.clone();
        let mut output: Vec<String> = Vec::new();

        let mut idx: usize = 0;
        while idx < input.len() {
            let node = input[idx].clone();
            // eprintln!("input[{}] = {:?}", idx, node);

            // Integer? Push to stack
            if node.node_type == AstNodeType::Integer {
                self.stack.push(node.node_value);
                idx += 1;
                continue;
            }

            // String? Push directly to output
            if node.node_type == AstNodeType::String {
                output.push(node.node_name);
                idx += 1;
                continue;
            }

            // Function def?
            //
            // `node_value` being `1` means the function is local-scope, anything
            // else is global-scope.
            if node.node_type == AstNodeType::FunctionDef {
                if node.node_value == 1 {
                    fn_scope.push(node.clone());
                } else {
                    self.function_defs.push(node.clone());
                }

                idx += 1;
                continue;
            }

            // Conditional?
            if node.node_type == AstNodeType::Conditional {
                if let Some(c) = self.stack.pop() {
                    // Here, we always assume that the first child of a Conditional
                    // is the truthy branch, and the second (and last) child is the
                    // falsy branch. `0` is falsy, everything else is truthy.

                    if c != 0 {
                        // true
                        match self
                            .execute_scoped(node.children[0].children.clone(), fn_scope.clone())
                        {
                            Err(err) => return Err(err),
                            Ok(s) => output.extend(s),
                        }
                    } else {
                        // false
                        match self
                            .execute_scoped(node.children[1].children.clone(), fn_scope.clone())
                        {
                            Err(err) => return Err(err),
                            Ok(s) => output.extend(s),
                        }
                    }
                } else {
                    return Err(InterpreterError::StackUnderflow);
                }

                idx += 1;
                continue;
            }

            // Loop?
            if node.node_type == AstNodeType::DoLoopBody {
                if let (Some(start), Some(end)) = (self.stack.pop(), self.stack.pop()) {
                    for i in start..=end {
                        let scope = {
                            let mut scope = fn_scope.clone();
                            scope.insert(
                                0,
                                AstNode::new_function(
                                    false,
                                    "i",
                                    vec![AstNode::new_integer(i as isize)],
                                ),
                            );

                            scope
                        };

                        match self.execute_scoped(node.children.clone(), scope) {
                            Err(err) => return Err(err),
                            Ok(s) => output.extend(s),
                        }
                    }
                } else {
                    return Err(InterpreterError::StackUnderflow);
                }

                idx += 1;
                continue;
            }

            // Loop?
            if node.node_type == AstNodeType::UntilLoopBody {
                let mut okay = true;
                while okay {
                    match self.execute_scoped(node.children.clone(), fn_scope.clone()) {
                        Err(err) => return Err(err),
                        Ok(s) => output.extend(s),
                    }

                    if let Some(res) = self.stack.pop() {
                        okay = res != 0;
                    }
                }

                idx += 1;
                continue;
            }

            // Lastly, if it's a word, attempt to execute it
            if node.node_type == AstNodeType::Word {
                let mut found: bool = false;

                // Check this scope's Forth definitions
                let mut scope_idx: usize = 0;
                while !found && scope_idx < fn_scope.len() {
                    let f_node = fn_scope[scope_idx].clone();
                    if f_node.node_type != AstNodeType::FunctionDef {
                        return Err(InterpreterError::InvalidNodeType(
                            String::from("Interpreter.execute_scoped"),
                            node.node_type,
                        ));
                    }

                    if node.node_name == f_node.node_name {
                        found = true;

                        match self.execute_scoped(f_node.children.clone(), fn_scope.clone()) {
                            Err(err) => return Err(err),
                            Ok(s) => output.extend(s),
                        }
                    }

                    scope_idx += 1;
                }

                // Check global Forth definitions
                let mut global_idx: usize = 0;
                while !found && global_idx < self.function_defs.len() {
                    let f_node = self.function_defs[global_idx].clone();
                    if f_node.node_type != AstNodeType::FunctionDef {
                        return Err(InterpreterError::InvalidNodeType(
                            String::from("Interpreter.execute_scoped"),
                            node.node_type,
                        ));
                    }

                    if node.node_name == f_node.node_name {
                        found = true;

                        match self.execute_scoped(f_node.children.clone(), fn_scope.clone()) {
                            Err(err) => return Err(err),
                            Ok(s) => output.extend(s),
                        }
                    }

                    global_idx += 1;
                }

                // Check native definitions
                let mut nf_idx: usize = 0;
                while !found && nf_idx < self.native_defs.len() {
                    let (nf_name, nf_func) = &self.native_defs[nf_idx];
                    if nf_name == &node.node_name {
                        found = true;

                        // Construct native data structure
                        let d = InterpreterNativeData {
                            stack: self.stack.clone(),
                            nodes: Vec::from(&input[idx..input.len()]),
                            function_defs: self.function_defs.clone(),
                            skip: 0,
                            output: String::new(),
                        };

                        match (nf_func)(d) {
                            Err(err) => return Err(err),
                            Ok(data) => {
                                self.stack = data.stack;
                                self.function_defs = data.function_defs;

                                idx += data.skip;
                                output.push(data.output);
                            }
                        }
                    }

                    nf_idx += 1;
                }

                if !found {
                    return Err(InterpreterError::InvalidWord(node.node_name.clone()));
                }

                idx += 1;
                continue;
            }

            // If we get here, we don't know how to handle this AstNodeType, so
            // error out
            return Err(InterpreterError::InvalidNodeType(
                String::from("Interpreter.execute_scoped"),
                node.node_type,
            ));
        }

        Ok(output)
    }
}

impl Debug for Interpreter {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let fdefs = self
            .function_defs
            .iter()
            .map(|t| t.node_name.clone())
            .collect::<Vec<String>>();

        let ndefs = self
            .native_defs
            .iter()
            .map(|(s, _)| s.clone())
            .collect::<Vec<String>>();

        f.debug_struct("Interpreter")
            .field("stack", &self.stack)
            .field("function_defs", &fdefs)
            .field("native_defs", &ndefs)
            .finish()
    }
}
