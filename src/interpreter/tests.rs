#![allow(unused_imports)]

use crate::alloc_prelude::*;
use crate::ast::{AstNode, AstNodeType};
use crate::interpreter::{Interpreter, InterpreterError};

#[test]
fn stack_push() {
    let mut interpreter = Interpreter::new();
    let res = interpreter.execute(vec![AstNode::new_integer(0xFF)]);

    assert!(res.is_ok());
    assert_eq!(interpreter.stack.pop(), Some(0xFF));
}

#[test]
fn call_forth() {
    let mut interpreter = Interpreter::new();
    let res = interpreter.execute(vec![AstNode::new_word(String::from("cr"))]);

    assert!(res.is_ok());
    assert_eq!(res.unwrap(), vec![String::from("\n")]);
    assert_eq!(interpreter.stack.pop(), None);
}

#[test]
fn call_native() {
    let mut interpreter = Interpreter::new();
    let res = interpreter.execute(vec![
        AstNode::new_integer(0x61),
        AstNode::new_word(String::from("emit")),
    ]);

    assert!(res.is_ok());
    assert_eq!(res.unwrap(), vec![String::from("a")]);
    assert_eq!(interpreter.stack.pop(), None);
}

#[test]
fn string_works() {
    let mut interpreter = Interpreter::new();
    let res = interpreter.execute(vec![AstNode::new_string(String::from("hello!"))]);

    assert!(res.is_ok());
    assert_eq!(res.unwrap(), vec![String::from("hello!")]);
    assert_eq!(interpreter.stack.pop(), None);
}

#[test]
fn conditional_truthy() {
    let mut interpreter = Interpreter::new();
    let res = interpreter.execute(vec![
        AstNode::new_integer(-1),
        AstNode::new_conditional(
            vec![AstNode::new_string(String::from("true!"))],
            vec![AstNode::new_string(String::from("false!"))],
        ),
    ]);

    assert!(res.is_ok());
    assert_eq!(res.unwrap(), vec![String::from("true!")]);
    assert_eq!(interpreter.stack.pop(), None);
}

#[test]
fn conditional_falsy() {
    let mut interpreter = Interpreter::new();
    let res = interpreter.execute(vec![
        AstNode::new_integer(0),
        AstNode::new_conditional(
            vec![AstNode::new_string(String::from("true!"))],
            vec![AstNode::new_string(String::from("false!"))],
        ),
    ]);

    assert!(res.is_ok());
    assert_eq!(res.unwrap(), vec![String::from("false!")]);
    assert_eq!(interpreter.stack.pop(), None);
}

#[test]
fn function_def_local_with_call() {
    let mut interpreter = Interpreter::new();
    let res = interpreter.execute(vec![
        AstNode::new_function(
            false,
            "test",
            vec![AstNode::new_string(String::from("hello!"))],
        ),
        AstNode::new_word(String::from("test")),
    ]);

    assert!(res.is_ok());
    assert_eq!(res.unwrap(), vec![String::from("hello!")]);

    // Check global function list for `test`
    for node in interpreter.function_defs {
        if node.node_name == "test" {
            assert!(false, "\"test\" was found in the global function list");
        }
    }
}

#[test]
fn function_def_global_with_call() {
    let mut interpreter = Interpreter::new();
    let res = interpreter.execute(vec![
        AstNode::new_function(
            true,
            "test",
            vec![AstNode::new_string(String::from("hello!"))],
        ),
        AstNode::new_word(String::from("test")),
    ]);

    assert!(res.is_ok());
    assert_eq!(res.unwrap(), vec![String::from("hello!")]);

    // Check global function list for `test`
    let mut found: bool = false;
    for node in interpreter.function_defs {
        if node.node_name == "test" {
            found = true;
        }
    }

    assert!(found, "\"test\" was not found in the global function list");
}

#[test]
fn loop_index() {
    let mut interpreter = Interpreter::new();
    let res = interpreter.execute(vec![
        AstNode::new_integer(10),
        AstNode::new_integer(0),
        AstNode::new_do_loop(vec![
            AstNode::new_word(String::from("i")),
            AstNode::new_word(String::from(".")),
        ]),
    ]);

    assert!(res.is_ok());
    assert_eq!(
        res.unwrap().join(" "),
        String::from("0 1 2 3 4 5 6 7 8 9 10")
    );
}

#[test]
fn loop_nested_index() {
    let mut interpreter = Interpreter::new();
    let res = interpreter.execute(vec![
        AstNode::new_integer(1),
        AstNode::new_integer(0),
        AstNode::new_do_loop(vec![
            AstNode::new_integer(5),
            AstNode::new_integer(1),
            AstNode::new_do_loop(vec![
                AstNode::new_word(String::from("i")),
                AstNode::new_word(String::from(".")),
            ]),
        ]),
    ]);

    assert!(res.is_ok());
    assert_eq!(res.unwrap().join(" "), String::from("1 2 3 4 5 1 2 3 4 5"));
}
