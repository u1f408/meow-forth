use crate::alloc_prelude::*;
use core::fmt::{self, Display};

/// Error type for the interpreter.
#[derive(Debug, Clone, PartialEq)]
pub enum InterpreterError {
    /// An operation requiring a value from the stack failed as the stack
    /// was empty.
    StackUnderflow,

    /// A word was encountered that the interpreter does not know about.
    InvalidWord(String),

    /// A parser error occurred.
    ParserError(crate::ast::ParseError),

    /// An invalid AST node type was passed to an interpreter function.
    InvalidNodeType(String, crate::ast::AstNodeType),

    /// An internal error occurred.
    ///
    /// This variant being returned signifies a bug in the interpreter.
    Internal,
}

impl Display for InterpreterError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::StackUnderflow => write!(f, "Stack underflow"),
            Self::InvalidWord(w) => write!(f, "{} ?", w),
            Self::ParserError(err) => write!(f, "Parser error: {}", err),
            Self::InvalidNodeType(cf, nt) => {
                write!(f, "Invalid AST node type passed to {}: {:?}", cf, nt)
            }
            Self::Internal => write!(f, "An internal interpreter error occurred (this is a bug!)"),
        }
    }
}

impl core::convert::From<core::option::NoneError> for InterpreterError {
    fn from(_: core::option::NoneError) -> InterpreterError {
        InterpreterError::Internal
    }
}
