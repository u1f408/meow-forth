use meow_forth::{ast::parser::AstParser, builtins::debug, Interpreter};
use rustyline::{error::ReadlineError, Editor};

pub static PKG_NAME: &'static str = env!("CARGO_PKG_NAME");
pub static PKG_VERSION: &'static str = env!("CARGO_PKG_VERSION");

fn main() {
    let parser = AstParser::new();
    let mut interpreter = Interpreter::new();
    debug::i_add_debug(&mut interpreter);

    println!("Hello from {}, version {}!", PKG_NAME, PKG_VERSION);
    println!("Ctrl-D to exit.");

    let mut rl = Editor::<()>::new();
    loop {
        match rl.readline(">>> ") {
            Ok(line) => {
                rl.add_history_entry(line.as_str());

                let parsed = match parser.parse(line.as_str()) {
                    Ok(parsed) => parsed,
                    Err(err) => {
                        eprintln!("parse error: {}", err);
                        continue;
                    }
                };

                match interpreter.execute(parsed) {
                    Ok(output) => {
                        if output.len() > 0 {
                            println!("{}", output.join(""))
                        }
                    }

                    Err(err) => {
                        eprintln!("execute error: {}", err);
                        continue;
                    }
                }
            }

            Err(ReadlineError::Interrupted) => {}
            Err(ReadlineError::Eof) => break,
            Err(err) => {
                eprintln!("readline error: {}", err);
                break;
            }
        }
    }
}
